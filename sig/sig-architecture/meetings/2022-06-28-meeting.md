# 架构SIG例会 2022-6-28 9:30-11:30(UTC+08:00)Beijing

## 议题(Agenda):
议题1、新建SIG仓申请：third_party_HDiffPatch<br>
议题2、规范部件仓名和目录申请：语言编译运行时<br>
议题3、规范部件仓名和目录申请：公共基础类库<br>
议题4、Misc子系统下各部件迁移至新子系统的申请<br>
议题5、开发板仓库结构设计及命名规范整改结果汇报<br>
议题6、sig仓孵化毕业：device_soc_amlogic，device_board_unionman，vendor_unionman<br>
议题7、CompileRuntime SIG 建仓申请<br>
议题8、规范部件仓名和目录申请:windowmanager<br>
议题9、扬帆sig仓孵化毕业：device_soc_rockchip，device_board_isoftstone，vendor_isoftstone<br>
议题10、致远sig仓孵化毕业：device_soc_allwinner，device_board_isoftstone，vendor_isoftstone<br>

## 与会人(Attendees)： 
任革林 [@im-off-this-week](https://gitee.com/im-off-this-week)<br>
董金光 [@dongjinguang](https://gitee.com/dongjinguang)<br>
赵文华 [@shidi_snow](https://gitee.com/shidi_snow)<br>
万承臻 [@wanchengzhen](https://gitee.com/wanchengzhen)<br>
强波   [@huawei_qiangbo](https://gitee.com/huawei_qiangbo)<br>
黄明龙 [@minglonghuang](https://gitee.com/minglonghuang)<br>
梁克雷 [@xzmu](https://gitee.com/xzmu)<br>

## 会议纪要(Notes)
**议题1、新建SIG仓申请：third_party_HDiffPatch**<br>
汇报人：韩锋<br>
1、暂不同意引入该开源软件。<br>
遗留问题：<br>
1、端侧和云侧升级方案需考虑拉通；<br>
2、应用升级方案与OpenHarmony系统层面的升级方案保持一致；<br>
3、PC侧方案需考虑跨平台（支持Windows、Linux、MacOS）；<br>
4、HDiffPatch提供的差分能力应该与升级文件格式无关。<br>

**议题2、规范部件仓名和目录申请：语言编译运行时**<br>
汇报人：李勇彪<br>
会议结论：<br>
1、同意RuntimeCompiler子系统改名为ArkCompiler子系统；<br>
2、同意js_runtime、ts2abc、toolchian、runtime_core（均为修改前名称）部件及其对应仓名和目录名的修改；<br>
3、js_runtime--> ets_runtime，ts2abc-->ets_frontend。<br>
遗留问题：<br>
无。<br>

**议题3、规范部件仓名和目录申请：公共基础类库**<br>
汇报人：黄慧进<br>
会议结论：<br>
1、同意Utils子系统改名为CommonLibrary；<br>
2、同意native、native_lite、ts_js_lang_library、memory、system_resources（均为修改前名称）部件及其对应仓名和目录名的修改；<br>
3、native-->c_utils，native_lite-->uitls_lite，ts_js_lang_library--> ets_utils。<br>
遗留问题：<br>
无。<br>

**议题4、Misc子系统下各部件迁移至新子系统的申请**<br>
汇报人：鲍亚永<br>
会议结论：<br>
1、同意迁移Misc子系统下各部件到各个新子系统下。<br>
遗留问题：<br>
1、命名规范，输入法框架子系统修改为输入法子系统，保持统一；<br>
2、锁屏管理服务部件命名同其他部件保持一致，修改为screenlock_fwk；<br>
3、time部件缺乏体现时区的概念，例如：timezone，包含时间、时区、定时器的概念在里面。<br>

**议题5、开发板仓库结构设计及命名规范整改结果汇报**<br>
汇报人：王少锋<br>
会议结论：<br>
以九联厂家A311D，开发板代号unionpi_tiger为例，建议修改点如下：<br>
1、device_soc_xxx仓<br>
   1）device_soc_xxx，其中xxx为soc厂家名称，如amloghic；<br>
   2）考虑可能有不同系列soc芯片的情况，建议增加一级区分了系列名称的目录；<br>
   3）common：soc系列相关的公共配置；<br>
   4）a311d/hardware：主要放置用户态GPU，显示、媒体HDI相关实现的内容，display、ge2d、gpu、isp、media。<br>
2、device_board_xxx仓<br>
   1）device_board_xxx，其中xxx为开发板厂家名称，如unionman；<br>
   2）unionpi_tiger：开发板目录名称；<br>
   3）unionpi_tiger/kernel/driver：原生的linux内核驱动；<br>
   4）unionpi_tiger/kernel/logo和unionpi_tiger/bootanimation建议合并至一个资源文件目录；<br>
   5）unionpi_tiger/hardware：开发板特有的HDI用户态实现，camera，LCD显示等；<br>
   6）unionpi_tiger/bootloader：uboot启动引导相关程序。<br>
3、vendor_xxx仓<br>
   1）vendor_xxx，其中xxx为开发板厂家名称，如unionman；<br>
   2）unionpi_tiger：建议修改为虚拟产品形态名称，不要和开发板名称重复，并补充针对具体产品的规范。<br>
4、该规范后续根据kernel SIG整改后的策略再刷新后发布。<br>
遗留问题：<br>
无。<br>

**议题6、sig仓孵化毕业：device_soc_amlogic，device_board_unionman，vendor_unionman**<br>
汇报人：于敏杰<br>
会议结论：<br>
1、同意毕业。<br>
遗留问题：<br>
1、内核patch继续整改，限期七月底前按规范修改完成。<br>

**议题7、CompileRuntime SIG 建仓申请**<br>
汇报人：彭彪<br>
会议结论：<br>
1、当前业务目标描述不清晰，需要重新修改制定后再评审是否建仓。<br>
遗留问题：<br>
无。<br>

**议题8、规范部件仓名和目录申请:windowmanager**<br>
汇报人：毛江平<br>
会议结论：<br>
1、同意window_manager部件及其仓名和目录名的修改：仓名由windowmanager 变为window_window_manager，目录名对应变化。<br>
遗留问题：<br>
无。<br>

**议题9、扬帆sig仓孵化毕业：device_soc_rockchip，device_board_isoftstone，vendor_isoftstone**<br>
汇报人：庞伟<br>
会议结论：<br>
1、同意毕业。<br>
遗留问题：<br>
1、内核patch继续整改，限期七月底前按规范修改完成。<br>

**议题10、致远sig仓孵化毕业：device_soc_allwinner，device_board_isoftstone，vendor_isoftstone**<br>
汇报人：庞伟<br>
会议结论：<br>
1、同意毕业。<br>
遗留问题：<br>
1、EULA 如何放置？如何对应具体的二进制文件？<br>
2、内核patch继续整改，限期七月底前按规范修改完成；<br>
3、继续规范仓库命名，如loader目录名变为bootloader等。<br>